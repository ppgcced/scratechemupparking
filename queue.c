#include "queue.h"
#include <stdio.h>
#include <stdlib.h>

Node *initNode(void *data) {
	Node *node = malloc(sizeof(Node));
	node->next = NULL;
	node->prev = NULL;
	node->data = data;
	return node;
}

Queue *initQ(unsigned int size) {
	Queue *result = malloc(sizeof(Queue));
	result->maxSize = size;
	result->length = 0;
	result->head = NULL;
	return result;
}

int enqueue(Queue *queue, void *data) {
	if (isFull(queue)) {
		return 1;
	}
	if (queue->head == NULL) {
		queue->head = initNode(data);
	} else {
		Node *p = queue->head;
		while (p->next != NULL) {
			p = p->next;
		}
		p->next = initNode(data);
		p->next->prev = p;
	}
	queue->length++;
	return 0;
}

void *dequeue(Queue *queue) {
	if (queue->head == NULL) {
		return NULL;
	}
	Node *p = queue->head;
	queue->head = p->next;
	void *result = p->data;
	free(p);
	queue->length--;
	return result;
}

unsigned int freeSpacesQ(Queue *queue) {
	return queue->length - queue->maxSize;
}

void forEach(Queue *queue, void (*each)(void*)) {
	Node *p = queue->head;
	int i = 0;
	while (p != NULL) {
		printf("V%d: %s", i++, (char*) p->data);
		each(p->data);
		p = p->next;
	}
}

int isEmpty(Queue *queue) {
	return queue->length == 0 ? 1 : 0;
}

int isFull(Queue *queue) {
	return queue->length == queue->maxSize ? 1 : 0;
}

void clear(Queue *queue) {
	Node *p = queue->head;
	while (p->next != NULL) {
		Node *tmp = p->next;
		free(p);
		p = tmp;
	}
	queue->head = NULL;
	queue->length = 0;
}
