/*
 * car.h
 *
 *  Created on: 27 de mar de 2017
 *      Author: clecius
 */

#ifndef CAR_H_
#define CAR_H_

typedef struct {
	char *plate;
	unsigned int changes;
} Car;

#endif /* CAR_H_ */
