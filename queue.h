/*
 * collection.h
 *
 *  Created on: 27 de mar de 2017
 *      Author: clecius
 */

#ifndef QUEUE_H_
#define QUEUE_H_

typedef struct node {
	struct node *prev;
	struct node *next;
	void *data;
} Node;

typedef struct {
	Node *head;
	unsigned int length;
	unsigned int maxSize;
} Queue;

/**
 * Inicializa uma fila
 */
Queue *initQ(unsigned int size);

/**
 * Enfileira uma informação
 */
int enqueue(Queue *queue, void *data);

/**
 * Remove da fila a proxima informação
 */
void *dequeue(Queue *queue);

/**
 *Retorna contagem de espaços livres
 */
unsigned int freeSpacesQ(Queue *queue);

/**
 * Executa uma função para cada um dos itens na fila
 */
void forEach(Queue *queue, void (*each)(void*));

int isEmpty(Queue *queue);

int isFull(Queue *queue);

void clear(Queue *queue);

#endif /* QUEUE_H_ */
