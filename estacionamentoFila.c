/*
 ============================================================================
 Name        : estacionamentoFila.c
 Author      : Ellen Wolf Roth
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <string.h>

struct estacionamento{
 char placa[7];
 int qtde_mov;
 struct estacionamento *prox;
};

typedef struct estacionamento e;

int tam;

e *alocarCarro();
void adicionarCarro(e *FILA);
void exibirEstacionamento(e *FILA);
int vazia(e *FILA);

//int main() {
//	struct estacionamento vagas[10];
//	int num;
//	char pesq[7];
//
//	while (num != 3) {
//		printf(
//				"\n Menu \n 1 - Entrada \n 2 - Saída \n 3 - Ver estacionamento \n 4 - Sair \n");
//		printf("\nDigite uma das opções: ");
//		scanf("%d", &num);
//		switch (num) {
//		case 1:
//			adicionarCarro(vagas);
//			break;
//		case 2:
//			break;
//		case 3:
//			exibirEstacionamento(vagas);
//			break;
//		case 4:
//			break;
//
//		default:
//			printf("\n\nNenhuma opção foi escolhida.");
//			break;
//		}
//	}
//
//	return 0;
//}

int vazia(e *FILA)
{
	if(FILA->prox == NULL)
		return 1;
	else
		return 0;
}

void adicionarCarro(e *FILA)
{
	e *novo=alocarCarro();
	novo->prox = NULL;

	if(vazia(FILA))
		FILA->prox=novo;
	else{
		e *tmp = FILA->prox;

		while(tmp->prox != NULL)
			tmp = tmp->prox;

		tmp->prox = novo;
	}
	tam++;
}

e *alocarCarro()
{
	e *novo=(e *) malloc(sizeof(e));
	if(!novo){
		printf("Estacionamento lotado!\n");
		exit(1);
	}else{
		printf("\n Digite a placa : ");
	    scanf("%s", &novo->placa);
	    novo->qtde_mov = 1;
		return novo;
	}
}

void exibirEstacionamento(e *FILA)
{
	if(vazia(FILA)){
		printf("Estacionamento vazio!\n\n");
	return ;
	}

	e *tmp;
	tmp = FILA->prox;
	printf("\n");
	while( tmp != NULL){
		printf("%s", tmp->placa);
		tmp = tmp->prox;
	}
	printf("\n ");
	int count;
	for(count=0 ; count < tam ; count++)
		printf("  ^  ");
	printf("\n");
	for(count=0 ; count < tam ; count++)
		printf("%5d", count+1);


	printf("\n\n");
}

