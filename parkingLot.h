#include "car.h"
#include "queue.h"

#ifndef PARKING_LOT_H_FILE

#define PARKING_LOT_H_FILE

void handleNewCar(Queue *queue, Queue *waitingQueue);
void withdrawCar(Queue *queue, Queue *waitingQueue);
void showAllParkedCars(Queue *queue);

#endif
