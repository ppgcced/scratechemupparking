#include <stdlib.h>
#include <stdio.h>

#include "parkingLot.h"
#include "queue.h"
void printStr(void *data) {
	printf("V: %s\n", (char*) data);
}
int main() {
	Queue *waitingQueue = initQ(10000);
	Queue *queue = initQ(10);
	printf("Length: %d | MaxSize: %d\n", queue->length, queue->maxSize);
	enqueue(queue, "Teste1");
	enqueue(queue, "Teste2");
//	enqueue(queue, "Teste3");
//	enqueue(queue, "Teste4");
//	enqueue(queue, "Teste5");
//	enqueue(queue, "Teste6");
	printf("Length: %d | MaxSize: %d\n", queue->length, queue->maxSize);
	forEach(queue, *printStr);
	char *result = (char*) dequeue(queue);
	printf("Resultado: %s\n", result);
	forEach(queue, *printStr);
	printf("Length: %d | MaxSize: %d\n", queue->length, queue->maxSize);
//	int num;
//	char pesq[7];
//
//	while (num != 4) {
//		printf(
//				"\n Menu \n 1 - Entrada \n 2 - Saída \n 3 - Ver estacionamento \n 4 - Sair \n");
//		printf("\nDigite uma das opções: ");
//		scanf("%d", &num);
//		switch (num) {
//		case 1:
//			handleNewCar(queue, waitingQueue);
//			break;
//		case 2:
//			withdrawCar(queue, waitingQueue);
//			break;
//		case 3:
//			showAllParkedCars(queue);
//			break;
//		case 4:
//			printf("\n\nEncerrando atividades.");
//			return 0;
//		default:
//			printf("\n\nNenhuma opção foi escolhida.");
//			break;
//		}
//	}

	return 0;
}

