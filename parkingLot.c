#include "parkingLot.h"
#include <stdlib.h>
#include <stdio.h>

#include "car.h"
#include "queue.h"

Car *allocateNewCar();
int isCarWithinParkingLot(Queue *queue, char *plate);
int isAtTheTop(Queue *queue, Car *car);
void replaceAllCarsAndRemoveOne(Queue *queue, Car *car);
int platesAreEqual(Car *c1, Car *c2);
int getPlateSize(Car *c);
void printCar(Car *car);

void showAllParkedCars(Queue *queue) {
	Node *node = queue->head;

	while (node != NULL && node->data != NULL) {
		printCar((Car *) node->data);
		printf("\n");

		node = node->next;
	}
}

void handleNewCar(Queue *queue, Queue *waitingQueue) {
	Car *car = allocateNewCar();

	if (isFull(queue)) {
		printf("Estacionamento lotado! Carro adicionado à fila de espera.\n");

		enqueue(waitingQueue, car);

		return;
	}

	enqueue(queue, car);

	printf("Carro %s entrou no estacionamento.\n", car->plate);
}

Car *allocateNewCar() {
	Car *car = (Car *) malloc(sizeof(Car));

	printf("\n Digite a placa : ");
	scanf("%s", car->plate);
	car->changes = 0;
	return car;
}

void withdrawCar(Queue *queue, Queue *waitingQueue) {

	char *plate;

	printf("\n Digite a placa : ");
	scanf("%s", plate);

	if (isCarWithinParkingLot(queue, plate)) {
		printf("Carro não está presente no estacionamento! Houve um engano.\n");
		return;
	}

	Car *car = (Car *) malloc(sizeof(Car));
	car->plate = plate;

	if (isAtTheTop(queue, car)) {
		car = dequeue(queue);
	} else {
		replaceAllCarsAndRemoveOne(queue, car);
	}

	printCar(car);
	printf(" saiu do estacionamento.\n");

	if (isEmpty(waitingQueue)) {
		car = dequeue(waitingQueue);

		enqueue(queue, car);
	}
}

int isCarWithinParkingLot(Queue *queue, char *plate) {

	Node *node = queue->head;

	Car *tmp = (Car *) malloc(sizeof(Car));
	tmp->plate = plate;

	while (node != NULL) {
		if (platesAreEqual((Car *) node->data, tmp)) {
			return 1;
		}
		node = node->next;
	}

	return 0;
}

int isAtTheTop(Queue *queue, Car *car) {
	Node *node = queue->head;

	while (node->next != NULL) {
		node = node->next;
	}

	if (node->data != NULL && platesAreEqual((Car *) node->data, car)) {
		return 1;
	}

	return 0;
}

void replaceAllCarsAndRemoveOne(Queue *queue, Car *car) {

	Queue *tmp = initQ(queue->maxSize);
	Node *node = queue->head;

	int found = 0;
	while (node != NULL && node->data != NULL) {
		if (found && platesAreEqual(car, (Car*) node->data)) {
			enqueue(tmp, node->data);
		} else {
			car = (Car*) node->data;
			found = 1;
		}
		node = node->next;
	}

	clear(queue);

	node = tmp->head;
	while (node != NULL && node->data != NULL) {
		enqueue(queue, node);
		node = node->next;
	}
}

int platesAreEqual(Car *c1, Car *c2) {

	int s1 = getPlateSize(c1);
	int s2 = getPlateSize(c2);

	if (s1 == s2) {
		for (int i = 0; i < s1; i++) {
			if (c1->plate[i] != c2->plate[i]) {
				return 1;
			}
		}
		return 0;
	}
	return 1;
}

int getPlateSize(Car *c) {
	char *p = c->plate;

	int i = 0;
	while (p[i] != '\0') {
		i++;
	}
	return i;
}

void printCar(Car *car) {
	printf("Carro %s [%d Movimentos]", car->plate, car->changes);
}
